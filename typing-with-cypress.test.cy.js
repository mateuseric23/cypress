/// <reference types="cypress" />

describe('Basic test', () => {

        it('Every basic element exists on mobile', () => {
            cy.viewport(1280, 720)
            cy.visit('https://codedamn.com')
    
        it('Login should work fine' , () => {
            //cy.visit('https://codedamn.com/login')
        
            cy.contains('Sign in').click({force:true})
    
            cy.get('[data-testid=username]').type('eric12', {force:true})
            cy.get('[data-testid=username]').type('eric12', {force:true})
            cy.get('[data-testid=password]').type('iniadalahpassword123', {force:true})
    
            cy.get('[data-testid=login]').click({force:true})
    
        })

        it('Login should display correct error' , () => {
            //cy.visit('https://codedamn.com/login')
    
            cy.contains('Sign in').click({force:true})
            
            cy.contains('Unable to authorize').should('not.exist')
            cy.get('[data-testid=username]').type('admin', {force:true})
            cy.get('[data-testid=username]').type('admin', {force:true})
            cy.get('[data-testid=password]').type('admin', {force:true})
    
            cy.get('[data-testid=login]').click({force:true})
    
    
        })
    })
})
    