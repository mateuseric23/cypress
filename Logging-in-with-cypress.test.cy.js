/// <reference types="cypress" />

describe('Basic test', () => {

        it('Every basic element exists on mobile', () => {
            cy.viewport(1280, 720)
            cy.visit('https://codedamn.com')

        it('Login should work fine' , () => {

            // TODO : set this as localstorage token for authentication
            const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVyaWMxMiIsIl9pZCI6IjYzMjE3Y2QyYTA2ODUzMDAwOTgyZDJkOSIsIm5hbWUiOiJhYmR1bCIsImlhdCI6MTY2MzE0MTY1MiwiZXhwIjoxNjY4MzI1NjUyfQ.F_JJed_ShIQ6TR7pBn9u9wY6TPWQiw4fXsx2Y63y3AE'
    
            cy.contains('Sign in').click({force:true})
            
            cy.get('[data-testid=username]').type('eric12', {force:true}) 
            cy.get('[data-testid=username]').type('eric12', {force:true})
            cy.get('[data-testid=password]').type('iniadalahpassword123', {force:true})
    
            cy.get('[data-testid=login]').click({force:true})
    
            cy.url().should('include', '/dashboard')
        })
    })
})
