it('New file feature works', () => {
    cy.visit('https://codedamn.com/playground/fxWwo77X8cPUe7QZjZhpd')

    cy.contains('Trying to establish').should('exist')
    cy.contains('Setting up the challenge', {timeout: 7 * 1000}).should('exist')
    cy.contains('Setting up the challenge', { timeout: 10 * 1000 }).should('not.exist')

    const fileName = Math.random().toString().slice(0, 3)

    cy.get('[data-testid=xterm]')
    .type('{ctrl}{c}')
    .type('touch testscript.${fileName}.js{enter}')

    cy.contains('testscript.${fileName}.js').rightclick()
    
    cy.contains('Rename File').click()

    cy.get('[data-testid=renamefilefolder]').type('new_.${fileName}.js')

    cy.contains('[data-testid=renamebtn]').click()
    cy.contains('testscript.${fileName}.js').should('not.exist')
    cy.contains('new_.${fileName}.js').should('not.exist')
})