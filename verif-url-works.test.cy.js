/// <reference types="cypress" />

describe('Basic test', () => {

        it('Every basic element exists on mobile', () => {
            cy.viewport(1280, 720)
            cy.visit('https://codedamn.com')

            it('Login page links work', () => {
                //Sign in page
                cy.contains('Sign in').click()
        
                cy.log('Going to forgot password')
        
                //Password reset page
                cy.contains('Forgot your password?').click({force:true})
        
                //Verify your page URL
                cy.url().should('include', '/password-reset')
        
                cy.url().then(value => {
                    cy.log('The current real is: ', cy.url())
                })
        
                // cy.log('Current URL = ', cy.url())
        
                //Go back, on the sign in page  
                cy.go('back')   
        
                cy.contains('Create one').click()
                cy.url().should('include', '/register')
        
        
        
            })
        })
})