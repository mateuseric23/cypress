/// <reference types="cypress" />

describe('Basic test', () => {
    it('Every basic element exists on mobile', () => {
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')

        it('Login page looks good', () => {   
            cy.contains('Sign in').click()
    
            cy.contains('Sign in to codedamn').should('exist')
            cy.contains('Create Free Account').should('exist')
            cy.contains('Create one').should('exist')
            cy.contains('Forgot your password?').should('exist')
            cy.contains('Remember me').should('exist')
           
    
    
        })

    })
})


