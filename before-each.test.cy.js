/// <reference types="cypress" />

describe('Basic Authenticated Desktop Tests', () => {

    // before(() => {
    //     cy.viewport(1280, 720)
    //     cy.then(() => {
    //         window.localStorage.setItem('__auth__token', token)
    //     })
    // })
   
    beforeEach(() => {
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
    })
    
    it('The webpage load, at least', () => {
        
        //way 1

        cy.contains('Explore All Learning Paths')

        //way 2

        cy.get('.relative > div > a')

        //way 3

        cy.get('[data-testid=homepage-cta]')

        //way 1

        cy.get('[data-testid=homepage-cta]').click()

        cy.contains('Why people love').should('exist')


    })

    it('Every basic element exists on mobile', () => {
        cy.viewport('iphone-xr')
        cy.visit('https://codedamn.com')
    })

    it('Login page looks good', () => {   
        cy.contains('Sign in').click()

        cy.contains('Sign in to codedamn').should('exist')
        cy.contains('Create Free Account').should('exist')
        cy.contains('Create one').should('exist')
        cy.contains('Forgot your password?').should('exist')
        cy.contains('Remember me').should('exist')
       


    })

    it('Login page links work', () => {
        //Sign in page
        cy.contains('Sign in').click()

        cy.log('Going to forgot password')

        //Password reset page
        cy.contains('Forgot your password?').click({force:true})

        //Verify your page URL
        cy.url().should('include', '/password-reset')

        cy.url().then(value => {
            cy.log('The current real is: ', cy.url())
        })

        // cy.log('Current URL = ', cy.url())

        //Go back, on the sign in page  
        cy.go('back')   

        cy.contains('Create one').click()
        cy.url().should('include', '/register')



    })

    it('Login should work fine' , () => {
        //cy.visit('https://codedamn.com/login')
    
        cy.contains('Sign in').click({force:true})

        cy.get('[data-testid=username]').type('eric12', {force:true})
        cy.get('[data-testid=username]').type('eric12', {force:true})
        cy.get('[data-testid=password]').type('iniadalahpassword123', {force:true})

        cy.get('[data-testid=login]').click({force:true})

    })

    it('Login should display correct error' , () => {
        //cy.visit('https://codedamn.com/login')

        cy.contains('Sign in').click({force:true})
        
        cy.contains('Unable to authorize').should('not.exist')
        cy.get('[data-testid=username]').type('admin', {force:true})
        cy.get('[data-testid=username]').type('admin', {force:true})
        cy.get('[data-testid=password]').type('admin', {force:true})

        cy.get('[data-testid=login]').click({force:true})


    })

    it('Login should work fine' , () => {

        // TODO : set this as localstorage token for authentication
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVyaWMxMiIsIl9pZCI6IjYzMjE3Y2QyYTA2ODUzMDAwOTgyZDJkOSIsIm5hbWUiOiJhYmR1bCIsImlhdCI6MTY2MzE0MTY1MiwiZXhwIjoxNjY4MzI1NjUyfQ.F_JJed_ShIQ6TR7pBn9u9wY6TPWQiw4fXsx2Y63y3AE'

        cy.contains('Sign in').click({force:true})
        
        cy.get('[data-testid=username]').type('eric12', {force:true}) 
        cy.get('[data-testid=username]').type('eric12', {force:true})
        cy.get('[data-testid=password]').type('iniadalahpassword123', {force:true})

        cy.get('[data-testid=login]').click({force:true})

        cy.url().should('include', '/dashboard')
    })

})
