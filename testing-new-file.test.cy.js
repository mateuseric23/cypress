/// <reference types="cypress" />

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVyaWMxMiIsIl9pZCI6IjYzMjE3Y2QyYTA2ODUzMDAwOTgyZDJkOSIsIm5hbWUiOiJhYmR1bCIsImlhdCI6MTY2MzE0MTY1MiwiZXhwIjoxNjY4MzI1NjUyfQ.F_JJed_ShIQ6TR7pBn9u9wY6TPWQiw4fXsx2Y63y3AE'

describe('Basic Authenticated Desktop Tests', () => {

    before(() => {
        cy.then(() => {
            window.localStorage.setItem('__auth__token', token)
        })
    })
   
    beforeEach(() => {
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
    })

    it('New file feature works', () => {
        cy.visit('https://codedamn.com/playground/fxWwo77X8cPUe7QZjZhpd')

        cy.contains('Trying to establish').should('exist')
        cy.contains('Setting up the challenge', {timeout: 7 * 1000}).should('exist')
        cy.contains('Setting up the challenge', { timeout: 10 * 1000 }).should('not.exist')

        const fileName = Math.random()

        cy.get('[data-testid=xterm]')
        .type('{ctrl}{c}')
        .type('touch testscript.js{enter}')

        cy.contains('testscript.${fileName}.js').should('exist')
    })

    

})
